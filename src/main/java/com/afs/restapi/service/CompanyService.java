package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.dto.CompanyUpdateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<CompanyResponse> getAll() {
        List<Company> companyList = companyRepository.findAll();
        return companyList.stream()
                .map(CompanyMapper::convertCompanyToCompanyResponse)
                .collect(Collectors.toList());
    }

    public List<CompanyResponse> getAll(Integer page, Integer pageSize) {
        return companyRepository.findAll(PageRequest.of(page, pageSize)).stream()
                .map(CompanyMapper::convertCompanyToCompanyResponse)
                .collect(Collectors.toList());
    }

    public CompanyResponse findById(Integer companyId) {
        Company company = findCompanyById(companyId);
        return CompanyMapper.convertCompanyToCompanyResponse(company);
    }

    public CompanyResponse create(CompanyCreateRequest companyCreateRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyCreateRequest, company);
        Company savedCompany = companyRepository.save(company);
        return CompanyMapper.convertCompanyToCompanyResponse(savedCompany);
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponse update(Integer companyId, CompanyUpdateRequest companyUpdateRequest) {
        Company company = findCompanyById(companyId);
        if (companyUpdateRequest.getName() != null) {
            company.setName(companyUpdateRequest.getName());
        }
        return CompanyMapper.convertCompanyToCompanyResponse(companyRepository.save(company));
    }

    public List<EmployeeResponse> getEmployees(Integer companyId) {
        return findCompanyById(companyId).getEmployees().stream()
                .map(EmployeeMapper::convertEmployeeToEmployeeResponse)
                .collect(Collectors.toList());
    }

    private Company findCompanyById(Integer companyId) {
        return companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
    }
}
