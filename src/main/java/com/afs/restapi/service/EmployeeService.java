package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return employeeRepository.findAllByStatusTrue().stream()
                .map(employee -> {
                    EmployeeResponse employeeResponse = new EmployeeResponse();
                    BeanUtils.copyProperties(employee, employeeResponse);
                    return employeeResponse;
                })
                .collect(Collectors.toList());
    }

    public Employee update(int id, EmployeeUpdateRequest employeeUpdateRequest) {
        Employee employee = getEmployeeById(id);
        if (employeeUpdateRequest.getAge() != null) {
            employee.setAge(employeeUpdateRequest.getAge());
        }
        if (employeeUpdateRequest.getSalary() != null) {
            employee.setSalary(employeeUpdateRequest.getSalary());
        }
        employeeRepository.save(employee);
        return employee;
    }

    public EmployeeResponse findById(int id) {
        Employee employee = getEmployeeById(id);
        return EmployeeMapper.convertEmployeeToEmployeeResponse(employee);
    }

    private Employee getEmployeeById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender).stream()
                .map(EmployeeMapper::convertEmployeeToEmployeeResponse)
                .collect(Collectors.toList());
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).stream()
                .map(EmployeeMapper::convertEmployeeToEmployeeResponse)
                .collect(Collectors.toList());
    }

    public Employee insert(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeCreateRequest);
        return employeeRepository.save(employee);
    }

    public void delete(int id) {
        Employee employee = getEmployeeById(id);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
