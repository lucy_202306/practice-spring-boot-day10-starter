package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyResponse;
import com.afs.restapi.entity.Company;
import org.springframework.beans.BeanUtils;


public class CompanyMapper {

    public static CompanyResponse convertCompanyToCompanyResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeesNumber(company.getEmployees().size());
        return companyResponse;
    }
}