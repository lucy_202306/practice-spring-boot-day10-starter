# day10-ORID

## O

1. code review. I found my a bad habit that alway forgot to delete some useless code.
2. Learned mapper and dto. I learned how to extract the mapper layer and copy the property value of one object to the corresponding property of another object by BeanUtils.copyProperty.
3. Learned retro and gave a feedback about last two weeks'study.
4. Gave our group's presentation on container.

## R

fruitful and relaxed.

## I

Retro is very important that can help us give teachers a exact feedback about our recent learning situation in time.

## D

1. The knowledge I learned this week is very useful. 
2. I need to clear my mind over the weekend. 